include { GUNZIP as GUNZIP_FASTA  } from '../../modules/nf-core/gunzip/main'
include { GUNZIP as GUNZIP_GFF    } from '../../modules/nf-core/gunzip/main'
include { GUNZIP as GUNZIP_GTF    } from '../../modules/nf-core/gunzip/main'
include { GUNZIP as GUNZIP_CFASTA } from '../../modules/nf-core/gunzip/main'
include { GUNZIP as GUNZIP_PFASTA } from '../../modules/nf-core/gunzip/main'
include { GUNZIP as GUNZIP_TFASTA } from '../../modules/nf-core/gunzip/main'
include { GFFREAD                 } from '../../modules/nf-core/gffread/main'
include { AGAT_CONVERTSPGXF2GXF   } from '../../modules/nf-core/agat/convertspgxf2gxf/main'
include { AGAT_CONVERTSPGFF2GTF   } from '../../modules/nf-core/agat/convertspgff2gtf/main'

workflow PREPARE_GENOME {

    ch_versions = Channel.empty()

    if (params.fasta) {
        if (params.fasta.endsWith('.gz')) {
            ch_fasta = GUNZIP_FASTA ( [ [:], params.fasta ] ).gunzip
        } else {
            ch_fasta = Channel.fromPath(params.fasta).map { [['id':params.species_code], it] }
        }
    }

    // if not rewrite to match with https://agat.readthedocs.io/en/latest/tools/agat_sp_extract_sequences.html

    if (params.gff) {
        if (params.gff.endsWith('.gz')) {
            ch_gff = GUNZIP_GFF ( [ [:], params.gff ] ).gunzip.map { it[1] }
        } else {
            ch_gff = Channel.fromPath(params.gff).map { [['id':params.species_code], it] }
        }
        
        cln_gff = AGAT_CONVERTSPGXF2GXF ( ch_gff ).output_gff
        ch_versions = ch_versions.mix(AGAT_CONVERTSPGXF2GXF.out.versions)

        // if there isn't a gtf file passed in params covert gff into gtf
        if (!params.gtf) { 
            ch_gtf = AGAT_CONVERTSPGFF2GTF( cln_gff ).output_gtf
            ch_versions = ch_versions.mix(AGAT_CONVERTSPGFF2GTF.out.versions)
        }
    } else {
        cln_gff = Channel.empty()
    }

    if (params.gtf) {
        if (params.gtf.endsWith('.gz')) {
          ch_gtf      = GUNZIP_GTF ( [ [:], params.gtf ] ).gunzip.map { it[1] }
        } else {
            ch_gtf = file(params.gtf) 
        }
    } else if (!params.gtf && !params.gff) {
        ch_gtf = Channel.empty()
    }

    if (params.cfasta) {
        if (params.cfasta.endsWith('.gz')) {
            ch_cfasta   = GUNZIP_CFASTA ( [ [:], params.cfasta ] ).gunzip.map { it[1] }
        } else {
            ch_cfasta = file(params.cfasta)  
        }
    } else {
        ch_cfasta = Channel.empty()
    }

    if (params.pfasta) {
        if (params.pfasta.endsWith('.gz')) {
            ch_pfasta   = GUNZIP_PFASTA ([ [:], params.pfasta ] ).gunzip.map { it[1] }
        } else {
            ch_pfasta = file(params.pfasta)  
        }
    } else {
        ch_pfasta = Channel.empty()
    }

    if (params.tfasta) {
        if (params.tfasta.endsWith('.gz')) {
            ch_tfasta   = GUNZIP_TFASTA ([ [:], params.tfasta ] ).gunzip.map { it[1] }
        } else {
            ch_tfasta = file(params.tfasta)  
        }
    } else {
        ch_tfasta = Channel.empty()
    }

    emit:
    fasta  = ch_fasta
    gff    = cln_gff
    gtf    = ch_gtf
    cfasta = ch_cfasta
    pfasta = ch_pfasta
    tfasta = ch_tfasta

    versions = ch_versions

}