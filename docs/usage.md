# New Genome Pipeline

WorkFlow for creating indices and summary stastics based on genomic, transcriptomic, and proteomic reference sequences.

## Usage 

```shell
usage: nextflow run main.nf -profile xanadu --fasta </path/to/*.fa> [--cfasta <> --gtf </path/to/*.gtf|*.gxf> --gff </path/to/*.*.gff> --pfasta </path/to/*.cds.fa> --plastid_fasta </path/to/*.??>] [options...]
  
  # Input Files #
  --fasta                 input reference sequence. (string [=])
  --pfasta                input protein sequences. (string [=])
  --cfasta                input coding sequence file (string [=])
  --tfasta                input transcriptomic sequence file (string [=])
  --gff                   input gff (string [=])
  --gtf                   input gtf (string [=])

  # Parameters # 
  # Quast
  --large_genome          runs a different algorith for larger genomes (boolean [=null])
  --quast_use_fasta       pass reference sequence to quast (boolean [=])
  --quast_use_gtf         pass gtf file to quast (boolean [=])  

  # Busco
  --public_database       path to lineage (string [=])
  --busco_lineages_path   pass ????? (string [=[]])
  --busco_config          path to busco configuration file (string [=[]])

  # Hisat2 Build
  --hisat2_build_memory     amount of memory for hisat2 to extract exons (numeric [=1])

  # EnTAP
  --enable_entap            run EnTAP (boolean [=true])
  --taxon                   specifiy taxonomy for species. Required w/ protein fasta (string [=])
  --tcoverage               minimum transcript coverage (numeric [=60])
  --qcoverage               minimum coverage to consider (numeric [=60])
  --entap_db                path to binary database (string [='/core/labs/Wegrzyn/EnTAP/EnTAP_v0.10.8/EnTAP/databases/entap_database.bin'])
  --eggnog_db               path to eggnog database in shared (string [='/isg/shared/databases/eggnog/4.1/eggnog.db'])
  --data_eggnog             path to eggnog clustered proteins pre-compiled for diamond (string [='/isg/shared/databases/eggnog/4.1/eggnog4.clustered_proteins.dmnd'])
  --diamond_db              path to diamond database (string [='/isg/shared/databases/Diamond/RefSeq/complete.protein.faa.216.dmnd'])

  # Quality Control Checks
  --min_total_length        minimum genome length or provides warning message (string [='200000000'])
  --min_number_genes        minimum number of genes from transcript file
  --min_n50                 minimum N50 score (string [='300'])
  --min_busco_score         minimum busco score (string [='50'])

  # Support for Intermediate files
  --output_busco            enable saving additional busco stats (boolean [=false])
  --output_quast            enable saving additional quast stats (boolean [=false])
  --output_gunzip           enable saving unzipped files (boolean [=false])
  --output_mode             method for outputting results (string [='copy'])

  # boiler plate options
  --outdir                Path to output directory. Appended to the front of auto-created path (string [='./'])

  # NextFlow options (NOTE called with one hypthen '-')
  -bg                     run workflow in the background
```


