/* Genomes WORKFLOW
* Slack  : 
* GitHub : https://gitlab.com/TreeGenes/New_Genome_Pipeline/-/tree/master/ftp_scripts
*
*/

//def summary_params = NfcoreSchema.paramsSummaryMap(workflow, params)
///////////////////////////////////////////////////////////////////////////////////////////
// valdiate input

WorkflowGenomes.initialise(params, log)

if (params.fasta) { ch_input = file(params.fasta) } else { exit 1, 'Need to specify path to genome file' }

// check input files exist
def checkPathParamList = [params.fasta, params.cfasta, params.gtf, params.gff, params.pfasta, params.plastid_fasta]
for (param in checkPathParamList) { if (param) { file(param, checkIfExists: true) } }
/////////////////////////////////////////////////////////////////////////////////////////////
// nf-core modules
include { CUSTOM_DUMPSOFTWAREVERSIONS } from '..//modules/nf-core/custom/dumpsoftwareversions/main.nf'
// local modules
include { README as README_ANNOTATION } from '../modules/local/readme/main.nf'

// local subworklfows
include { PREPARE_GENOME } from '../subworkflows/local/prepare_genomes'
include { FASTA_BLAST_BWA_GMAP_HISAT2 as FASTA_BUILDER } from '../subworkflows/local/fasta_blast_bwa_gmap_minimap2_hisat2_genome_index'
include { CFASTA_BLAST_QUAST as CFASTA_BUILDER } from '../subworkflows/local/cfasta_blast_quast'
include { PFASTA_BLAST_DIAMOND_BUSCO as PFASTA_BUILDER } from '../subworkflows/local/pfasta_blast_diamond_busco'
include { TFASTA_GMAP_AGAT as TFASTA_BUILDER } from '../subworkflows/local/tfasta_gmap_agat'

workflow GENOME_DATABASE_BUILDER {
    // trash bin collecting software versions from subworkflows and modules
    ch_versions = Channel.empty()

    if (params.input == 'csv') {
        CHECK_INPUT ()
    
    
    } else {
        
    }
    // rename the file to the standard way so that busco and quast output match

    PREPARE_GENOME ()
    ch_versions = ch_versions.mix(PREPARE_GENOME.out.versions)

    // check GFF ID's match coding sequece fasta
    PREPARE_GENOME.out.gff.merge(PREPARE_GENOME.out.cfasta).map { WorkflowGenomes.checkGFF3MatchesFASTA(log, it[1], it[2]) }

    // FASTA_BUILD
    FASTA_BUILDER ( PREPARE_GENOME.out.fasta ,  PREPARE_GENOME.out.gtf )
    ch_versions = ch_versions.mix(FASTA_BUILDER.out.versions)

    // Check Genome QUAST and BUSCO report meets minimum requirements
    FASTA_BUILDER.out.quast.map { WorkflowGenomes.checkQuastStatistics(params, it, log, 'genome') }
    FASTA_BUILDER.out.busco.map { WorkflowGenomes.checkBuscoScore(params, it, log) } 

    // coding sequence fasta
    if ( params.cfasta ) { 
        CFASTA_BUILDER ( PREPARE_GENOME.out.cfasta , PREPARE_GENOME.out.fasta, PREPARE_GENOME.out.gtf )
        ch_versions = ch_versions.mix(CFASTA_BUILDER.out.versions)        
        }
    
    // create database based on protein sequences
    if ( params.pfasta ) { 
        PFASTA_BUILDER ( PREPARE_GENOME.out.pfasta ) 
        ch_versions = ch_versions.mix(PFASTA_BUILDER.out.versions)

        PFASTA_BUILDER.out.busco.map { WorkflowGenomes.checkBuscoScore(params, it, log) }
        }

    // Transcript fasta
    if ( params.tfasta ) { 
        TFASTA_BUILDER ( PREPARE_GENOME.out.tfasta , PREPARE_GENOME.out.fasta, PREPARE_GENOME.out.gff, FASTA_BUILDER.out.gmap ) 
        ch_versions = ch_versions.mix(TFASTA_BUILDER.out.versions)
        }

    // Create README.md based on coding and protein sequences
    if ( params.cfasta && params.pfasta ) { README_ANNOTATION (Channel.value('annotations'), CFASTA_BUILDER.out.quast, PFASTA_BUILDER.out.busco.map{it[1]}, []) }

    // write YAML versions
    CUSTOM_DUMPSOFTWAREVERSIONS (
        ch_versions.unique().collectFile(name: 'collated_versions.yml')
    )
}