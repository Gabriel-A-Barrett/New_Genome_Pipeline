import nextflow.Nextflow

class WorkflowGenomes {

    //
    // Check and validate parameters
    //
    public static void initialise(params, log) {

        if (!params.species_code || params.species_code == 'null') {
            log.error "must specify species_code with e.g. '--species_code aalba5' or via a detectable config file."
            System.exit(1)
        }
        if (!params.version || params.version == 'null') {
            log.error "must specify version with e.g. '--version v1.1' or via a detectable config file."
            System.exit(1)
        } 
        if (!params.fasta || params.fasta == 'null') {
            log.error "must specify fasta with e.g. '--fasta /path/to/fasta' or via a detectable config file."
            System.exit(1)
        }
        if (params.enable_entap && params.taxon == 'null') {
            log.error "must specify taxon with e.g. '--taxon id' or via a detectable config file like within nextflow.config"
            System.exit(1)
        }
    }

    public static ArrayList checkGFF3MatchesFASTA(log, gff3, pfasta) {

        def gff3File = gff3
        def fastaFile = pfasta

        // Read GFF3 file and extract IDs
        def gff3Ids = gff3File.withReader { reader ->
            reader.findAll { line ->
                line.trim().contains('transcript') // Modify this condition as per your GFF3 format
            }.collect { line ->
                line.split('\t')[8].split(';')[0].split('=')[1]
            }
        }

        // Read FASTA file and extract IDs
        def fastaIds = fastaFile.withReader { reader ->
            reader.findAll { line ->
                line.trim().startsWith('>') // Modify this condition as per your FASTA format
            }.collect { line ->
                line.substring(1).split(' ')[0]
            }
        }

        // Compare IDs
        def matchingIds = gff3Ids.intersect(fastaIds)
        def mismatchedIds = gff3Ids - matchingIds

        // Check if there are any mismatched IDs
        if (mismatchedIds.isEmpty()) {
            log.info "[checkGFF3MatchesFASTA] All IDs in the GFF3 file match the FASTA file."
        } else {
            log.warn "[checkGFF3MatchesFASTA] Mismatched IDs found:"
            mismatchedIds.each { id ->
                log.warn "[checkGFF3MatchesFASTA] ${id}"
            }
        }
    }

    public static ArrayList checkQuastStatistics(params, quast_log, log, type) {
        //def min_total_length = '200000000'
        //def min_number_genes = '10000'
        //def min_n50          = '300'

        def total_length_pattern  = /^Total\s+length\s+(\d+)\s*/
        def total_genes_pattern   = /^#\s+contigs\s+(\d+)\s*/
        def n50_pattern           = /^N50\\s+(\d+)\s*/

        quast_log.eachLine { line ->
            if ( type == 'genome' ) {
                def total_length_matcher = line =~ total_length_pattern
                if (total_length_matcher) {
                    
                
                    def total_size = total_length_matcher[0][1].toInteger()
                    if (total_size < params.min_total_length.toInteger()) {
                        log.error 'genome size is smaller than 200Mb. Please check that the .fasta is correct'
                        System.exit(1)
                        }
                    }
            } else if ( type == 'cds' ) {
                def total_genes_matcher = line =~ total_genes_pattern
                if (total_genes_matcher) {
                    
                    def total_genes = total_genes_matcher[0][1].toInteger()

                    if (total_genes < params.min_number_genes.toInteger()) {
                        log.error 'number of genes in .fasta is less than 10,000. Please check that the .cds.fasta is correct'
                        System.exit(1)
                        }
                    }
                def n50_matcher = line =~ n50_pattern
                if (n50_matcher) {

                    def n50 = n50_matcher[0][1].toInteger()

                    if (n50 < params.min_n50.toInteger()) {
                        log.error 'n50 in .cds.fasta is less than 300bp. Please check that the .cds.fasta is correct'
                        System.exit(1)
                    }
                }
            } else {
                log.error 'issue with type in ./workflows/genomes.nf from checkQuastStatistics '
                System.exit(1)
            }
        } 
    }
    public static ArrayList checkBuscoScore(params, busco_log, log) {
            
        def busco_score_pattern = /^\b(\d+\.\d+)%/

        def matcher = busco_log =~ busco_score_pattern
        if (matcher.find()) {
            def completeness = matcher.group(1).toFloat()

            if (completeness < params.min_busco_score.toFloat()) {
                log.error 'busco score below ' + params.min_busco_score + ' please check files'
                System.exit(1)
            }
        }
    }

        /*public static ArrayList writeAnnotationReadMe(params, quast, busco, log) {

            def outputFile = new File("README.md")

            def entapConfig = "#########\nQuast Results\n#########\n" + quast_file.text

            outputFile.write(combinedContent)

        }*/




}
