#SBATCH -n 1
#SBATCH -c 1
#SBATCH --mem=3G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

module load nextflow

nextflow run https://gitlab.com/Gabriel-A-Barrett/New_Genome_Pipeline.git -r dev -resume --species_code Potr --version v4.1\
 --fasta ../Potr/v4.1/genome/Potr.4_1.fa --gff ../Potr/v4.1/annotation/Potr.4_1.gff\
 --pfasta ../Potr/v4.1/annotation/Potr.4_1.pep.fa --cfasta ../Potr/v4.1/annotation/Potr.4_1.cds.fa\
 --taxon 'populus'\
 -profile xanadu -bg > runGenomes.log 2> runGenomes.err