# TreeGenes Genomes

A general pipeline/how-to for converting annotated genome files to more usable formats.

**FTP directory**: `/isg/treegenes/treegenes_store/FTP/Genomes`

## What to input for parameters: *--species_code & --version*

### Four Letter Code Naming Scheme (*--species_code*)

Species are organized by four letter species code names, which are typically the first two letters of the species name combined with the first two letters of the genus name. For example, _Betula nana_ will become `Bena`. In some cases, if the four letter code is already in use by another species, the new species' code will become the first two letters of the genus name, combined with the second and third letters of the species name. In this case, _Abies balsamea_ becomes `Abal` since `Abba` is already taken. 

In order to keep track of these codes, there is an entry in the `organismprop` table for each species. Example query below:
```
select o.genus "Genus", o.species "Species", op.value "Code" from chado.organism o 
 join chado.organismprop op on o.organism_id=op.organism_id
 where op.type_id=52307;

      Genus      |    Species    | Code 
-----------------+---------------+-------
 Calocedrus      | decurrens     | Cade
 Cryptomeria     | japonica      | Crja
 Larix           | decidua       | Lade
 Larix           | kaempferi     | Laka
 ...
```
### Version Naming Scheme (*--version*)

Version numbers are typically determined based on the assignment provided by the genomes respective authors. In the case that there is no version number provided by the authors than start with v1.0 and increment upwards. If there is a genome done on the *same* species but by *different* groups than make sure the more complete genome has the higher version number. 

### See [usage.md](./docs/usage.md) for additional information on parameters

----
## File Permissions
All files in the FTP directory, for the purposes of reachability from nginx (aka the website) and to be able to be modified by lab members, **must** have the following permissions:

 - Group: `treegenes`
 - Permissions: `775` or `drwxrwxr-x` (directories) or `rwxrwxr-x` (regular files)

The file owner can be whoever, although it is preferred to be `tg-nginx`.

To set these permissions:
  - For files: `chgrp treegenes filename` & `chmod 775 filename`
  - For directories (recursive) `chgrp -R treegenes directory/` & `chmod -R 775 directory/`

If you're not sure which files you own,  you can get a list:
`find /isg/treegenes/treegenes_store/FTP/ -user $USER@cam.uchc.edu`
For more detailed information on the files you own:
`find /isg/treegenes/treegenes_store/FTP/ -user $USER@cam.uchc.edu -exec stat -c '%a %G %n' {} \;`

## Tripal Sequence Similarity Search (TESEQ)

TSeq (Tripal Sequence Similarity Search) provides users a list of available pre-indexed protein and nucleotide databases to run their DIAMOND or BLAST queries against.

Once the indexing stage is complete (see README.md), the database should be added to TSeq.
This can be done one of two ways:
1.  Filling out the form manually at `admin/tripal/extension/tseq/add_db`
2.  Importing a CSV sheet with 1 or more databases listed.

Required information is:
1. Type of database (Protein, Nucleotide, etc.)
2. Name of databae (human readable, will show up in a dropdown list)
3. Version of database
4. Category (Most likely 'Standard', see TSeq documentation if using additional categories)
5. File location (Must be accessible on disk to the user who is running the command. Note that
        this refers to the user that Tripal Remote Job logs into as, assuming the job is being run on a remote machine)
6. Web Location (Optional) - If the original non-indexed file is available for download, list it here
7. Count - the number of sequences/scaffolds held in the database. This can be found by running `grep ">" file.fa | wc -l` on the original sequence file