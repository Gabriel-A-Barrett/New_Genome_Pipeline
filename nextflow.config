params {

    ///////////////////////////
    // Required Parameters
    fasta         = null
    species_code  = null
    version       = null // string '' containing v in the front
    ///////////////////////////

    cfasta        = null
    gtf           = null
    gff           = null
    pfasta        = null
    plastid_fasta = null

    // pass additional files to QUAST used with cfasta
    large_genome    = null
    quast_use_fasta = false
    quast_use_gff   = false

    // BUSCO
    busco_lineage = 'embryophyta_odb10'
    busco_lineages_path = [] //'/isg/shared/databases/BUSCO/odb10/lineages/embryophyta_odb10/'
    busco_config = []
    
    // histat2 build
    hisat2_build_memory = 1

    // EnTAP
    enable_entap  = true
    taxon         = null
    tcoverage     = 60
    qcoverage     = 60
    entap_db      = '/core/labs/Wegrzyn/EnTAP/EnTAP_v0.10.8/EnTAP/databases/entap_database.bin'
    eggnog_db     = '/isg/shared/databases/eggnog/4.1/eggnog.db'
    data_eggnog   = '/isg/shared/databases/eggnog/4.1/eggnog4.clustered_proteins.dmnd'
    diamond_db    = '/isg/shared/databases/Diamond/RefSeq/complete.protein.faa.216.dmnd'

    //////////////////////////////////
    // Quality Control Checks (Groovy Functions in ./lib)
    // QUAST
    min_total_length = '200000000'
    min_number_genes = '10000'
    min_n50          = '300'
    // BUSCO
    min_busco_score  = '50'

    // Boiler Plate Options
    outdir        = './'
    // saving intermediate files
    output_busco  = false
    output_quast  = false
    output_gunzip = false
    output_mode   = 'copy'
    
    // Don't Touch
    custom_config_version      = 'master'
    custom_config_base         = "https://raw.githubusercontent.com/nf-core/configs/${params.custom_config_version}"
    config_profile_description = null
    config_profile_contact     = null
    config_profile_url         = null
    config_profile_name        = null

}

docker.registry = 'quay.io'

// Load base.config by default for all pipelines
includeConfig 'conf/base.config'

// Load nf-core custom profiles from different Institutions
try {
    includeConfig "${params.custom_config_base}/nfcore_custom.config"
} catch (Exception e) {
    System.err.println("WARNING: Could not load nf-core/config profiles: ${params.custom_config_base}/nfcore_custom.config")
}

profiles {
    debug { process.beforeScript = 'echo $HOSTNAME' }
    
    test   { includeConfig 'conf/test.config' }

    treegenes { includeConfig 'conf/treegenes.config' }
}

// Load modules.config for DSL2 module specific options
// where to put output and additional arguments for specific modules
includeConfig 'conf/modules.config'

env {
    PYTHONNOUSERSITE = 1
    R_PROFILE_USER   = "/.Rprofile"
    R_ENVIRON_USER   = "/.Renviron"
    JULIA_DEPOT_PATH = "/usr/local/share/julia"
}

// Capture exit codes from upstream processes when piping
process.shell = ['/bin/bash', '-euo', 'pipefail']

manifest {
    name            = 'TreeGenes/genome_database'
    author          = 'Gabriel Barrett'
    homePage        = 'https://gitlab.com/Gabriel-A-Barrett/New_Genome_Pipeline/-/tree/dev/ftp_scripts'
    description     = 'genome_database'
    mainScript      = 'main.nf'
    nextflowVersion = '!>=21.10.3'
    version         = '1.0dev'
}


// Function to ensure that resource requirements don't go beyond
// a maximum limit
def check_max(obj, type) {
    if (type == 'memory') {
        try {
            if (obj.compareTo(params.max_memory as nextflow.util.MemoryUnit) == 1)
                return params.max_memory as nextflow.util.MemoryUnit
            else
                return obj
        } catch (all) {
            println "   ### ERROR ###   Max memory '${params.max_memory}' is not valid! Using default value: $obj"
            return obj
        }
    } else if (type == 'time') {
        try {
            if (obj.compareTo(params.max_time as nextflow.util.Duration) == 1)
                return params.max_time as nextflow.util.Duration
            else
                return obj
        } catch (all) {
            println "   ### ERROR ###   Max time '${params.max_time}' is not valid! Using default value: $obj"
            return obj
        }
    } else if (type == 'cpus') {
        try {
            return Math.min( obj, params.max_cpus as int )
        } catch (all) {
            println "   ### ERROR ###   Max cpus '${params.max_cpus}' is not valid! Using default value: $obj"
            return obj
        }
    }
}