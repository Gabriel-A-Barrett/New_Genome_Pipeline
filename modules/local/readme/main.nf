process README {
    tag "$type"
    label 'process_low'

    conda "conda-forge::sed=4.7"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/ubuntu:20.04' :
        'nf-core/ubuntu:20.04' }"

    input:
    val type
    path quast
    path busco, stageAs: 'busco.txt'
    path agat

    output:
    path 'README.md', emit: readme

    when:
    task.ext.when == null || task.ext.when

    script:
    def args = task.ext.args ?: ''
    if (type == 'genome') {
        """
        (cat $quast; echo "########"; echo "Quast Results"; echo "########"; cat $busco) > README.md
        """ 
    } else if (type == 'alignments') {
        """
        cat $agat > README.md
        """
    } else if (type == 'annotations') {
        // TODO: change quast output to match 
        """
        (echo "########"; echo "QUAST Results"; echo "########"; echo "" ;cat $quast; echo ""; echo "########"; echo "BUSCO Results"; echo "########"; echo "" ;cat $busco) > README.md
        # sed 's/# contigs (>= 0 bp)'
        
        """
    } else {
        exit(1)
    }
}
