process GMAP {
    tag "$tfasta"
    label 'process_medium'

    conda "bioconda::gmap"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/gmap:2021.08.25--pl5321h67092d7_2' :
        'quay.io/biocontainers/gmap:2021.02.22--pl526h2f06484_0' }"

    input:
    path (tfasta)
    path (gmap_db)

    output:
    path '*.gff3', emit: gff3
    //path "versions.yml" , emit: versions

    when:
    task.ext.when == null || task.ext.when

    script:
    def args = task.ext.args ?: ''
    """
    gmap -D ./ -d $gmap_db \\
    --nthreads=${task.cpus} \\
    $args \\
    $tfasta > ${tfasta.getSimpleName()}.gmap.gff3
    """
}
