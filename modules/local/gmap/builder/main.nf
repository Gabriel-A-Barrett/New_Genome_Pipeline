process GMAP_BUILD {
    tag "$fasta"
    label 'process_medium'

    conda "bioconda::gmap"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/gmap:2021.08.25--pl5321h67092d7_2' :
        'quay.io/biocontainers/gmap:2021.02.22--pl526h2f06484_0' }"

    input:
    path fasta

    output:
    path "${fasta.getSimpleName()}.gmap", emit: gmap
    //path "versions.yml" , emit: versions

    when:
    task.ext.when == null || task.ext.when

    script:
    def args = task.ext.args ?: ''
    """
    gmap_build -D ./ -d "${fasta.getSimpleName()}.gmap" $fasta
    
    
    """
}
