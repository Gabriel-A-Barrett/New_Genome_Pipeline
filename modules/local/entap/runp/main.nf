process ENTAP_RUNP {
    tag "$pfasta"
    label 'process_super'

    module '/isg/shared/modulefiles/EnTAP/0.10.8:/isg/shared/modulefiles/anaconda/2.4.0:/isg/shared/modulefiles/perl/5.30.1:/isg/shared/modulefiles/diamond/2.0.6:/isg/shared/modulefiles/interproscan/5.25-64.0:/isg/shared/modulefiles/TransDecoder/5.3.0:/isg/shared/modulefiles/eggnog-mapper/0.99.1'
    //container "systemsgenetics/entap:flask"

    input:
    path (pfasta)
    path (config)
    path (diamond_db)

    output:
    path("entap_outfiles/*"),    emit: output
    path("entap_outfiles/final_results/log*"), emit: log

    //path "versions.yml" , emit: versions

    when:
    task.ext.when == null || task.ext.when

    script:
    def args = task.ext.args ?: ''
    """
    diamond_db=\$(readlink -f ${diamond_db})
    EnTAP --runP --ini ${config} -i ${pfasta} -d \$diamond_db --threads ${task.cpus} --state 1+
    """
}
